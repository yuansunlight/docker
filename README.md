# Quick start

```
Usage:

1 . cp .env.example .env 

2 . add project

3 . add nginx conf in conf.d
```

```
Command:

docker-compose up 前台启动

docker-compose up -d 后台启动

docker-compose stop 暂停容器

docker-compose start 启动容器

docker-compose down -v 停止并删除容器

docker exec -it [container] sh 进入容器
```

```
Enable Xdebug:

1 . Preferences -> Set Debug port 9001

2 . Select path mapping 

That's ok!
```
